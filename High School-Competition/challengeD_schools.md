<img src="../images/nust.png"/>
<br/>

Challenge for High School Teams
============================

<p style='text-align: justify; font-size: 1.2em;'><b>Problem D: Point Calculator  </b><br><br/>Consider a point calculation system for <b>grade 12 learners in Namibia</b>. Your task is to create a system that take in a learner’s names and the 6 subjects written plus the symbol obtained, then calculate the total points obtained. The symbols can either be <b>1-4 or U for High level and A*-G or U for ordinary level</b> with respective points give in table below, it’s up to your system to figure this out. However remember that incase the learner failed English then the learner gets 0 points and a fail. Additional requirements are as follows:
<ul>
<li>Only valid symbols are accepted   </li>
<li>After calculation the system should indicate whether the learner is eligible for </li>
<li>tertiary provided they have 25 points and at least an E in English  </li>
<li>Symbols can be entered in either case (lower or upper case)  </li>
<li>If student skipped the exam for a given subject then a – (dash) is entered</li>
</ul>
<br/>
<table>
         <tr>
            <th colspan="2">Ordinary Level  </th>
            <th colspan="2">Higher Level  </th>
         </tr>
         <tr><td>A+</td><td> 8 </td><td>	1 </td><td>	10 </td></tr>
					<tr><td>A </td><td>	7 </td><td>	2 </td><td>	9 </td></tr>
					<tr><td>B </td><td>	6 </td><td>	3 </td><td>	8 </td></tr>
					<tr><td>C </td><td>	5 </td><td>	4 </td><td>	7 </td></tr>
<tr><td>D </td><td>	4 </td><td>	U </td><td>	0 </td></tr>
<tr><td>E </td><td>	3</td><td></td><td></td></tr>	  	  
<tr><td>F </td><td>	2 </td><td></td><td></td></tr>	  	  
<tr><td>G </td><td>	1</td><td></td><td></td></tr> 	  	  
<tr><td>U </td><td>	0</td><td></td><td></td></tr>
      </table>
</p>

<p style='text-align: justify; font-size: 1.2em;'> <br/>
Additionally the system should ask the learners their preferred 2 fields of study at tertiary and then indicate whether they would be admitted into that field or not. If not admitted the system should provide a reason. Below are all the fields of study at NUST and their admission requirements    </p>

| **Fields of Study/Faculty**            | **Required  points**    | **Additional requirements**                                  |
| -------------------------------------- | ----------------------- | :----------------------------------------------------------- |
| Management Sciences                    | 26 points in 5 subjects | D symbols in English <br/>E symbol in Mathematics            |
| Human Sciences                         | 26 points in 5 subjects | C symbols in English                                         |
| Computing and Informatics              | 30 points in 5 subjects | C symbols in Mathematics                                     |
| Engineering Sciences                   | 37 points in 5 subjects | Mathematics L3,  <br/>Level 4 for English and Physical Science |
| Health and Applied Sciences            | 30 points in 5 subjects | C symbols in English <br/>D symbols in Biology, Mathematics and Physical Science |
| Natural Resources and Spatial Sciences | 30 points in 5 subjects | C symbols in English <br/>D symbols in Geography and Mathematics |





<p style='text-align: justify; font-size: 1.2em;'>
  <b>Sample/Example  </b> 
  <br/>Enter learner’s full names: Joshua Mahepo
	<br/>Enter learner’s gender: Male  
	<br/>Enter subjects followed by symbols:  
	<br/>English 2  
	<br/>Khoekhoegowab  1  
	<br/>Biology B  
	<br/>Chemistry C  
	<br/>Mathematics B  
 <br/>Agriculture A  
<br/>Field of study 1: Engineering Sciences  
<br/>Field of study 2: Medicine  
<br/><b>Outcome  </b>
Dear Mr. Joshua  Mahepo, you have 38 points in 5 subjects. Congratulations you are eligible for tertiary studies in Namibia.  
<br/><b>Field of study  1</b>: Unfortunately you have not been admitted into Engineering Sciences.  
<br/><b>Comment</b>: Physical Science missing on list of subjects  
<br/><b>Field of study  2</b>: Medicine is not offered at NUST
</p>  











