![NUST Logo](../images/nust.png)

Challenge for High School Teams
============================

<p style='text-align: justify; font-size: 1.2em;'><b>Problem B: ID Numbers and Passport  </b><br><br/>The Ministry of Home Affairs and Immigration would like to introduce an automated id number and passport generation system. The system takes into consideration the following: </p>

  <ul style='text-align: justify; font-size: 1.2em;'>
    <li>To generate the <b>id number</b> it takes two last digits from the Year of birth, two digits for the month of birth plus two digits for the date of birth, then finally adds a five-digit postfix number.</li>
    <li>To generate the passport is similar, the system takes the first part from the id number with the year, month and date  then it adds the <b>"P-NAM"</b> prefix and finally adds the last two digits from the id number's five-digit postfix number</li>
</ul>
<p style='text-align: justify; font-size: 1.2em;'>Your task is to create a program that can achieve the above requirements to assist the ministry automate it’s operations 
</p>

<p style='text-align: justify; font-size: 1.2em;'><b>Input </b>  <br/>
Input is a single line with the person's first name, date of birth<b>(format YYYY-MM-DD)</b> and five-digit postfix through keyboard input, all separated by single spaces </p>

<p style='text-align: justify; font-size: 1.2em;'><b>Output </b>  <br/>
Output a customized welcome message using the provided name in the first line and then followed by the two lines with the generated ID number and Passport code in separate lines respectively </p>

 

<p style='text-align: justify; font-size: 1.2em;'><b>Sample Input 1 :</b>  <br/>
Kandjeke 1992-05-26 0033-8<br/><b>Sample Output 1 :</b><br/>Good day Kandjeke welcome to the Ministry of Home Affairs and Immigration automated system. 
<br/>Your ID number is : <i>920526 0033-8 </i> 
<br/>Passport code : <i>P-NAM 920526 3-8 </i> 
</p>  

<p style='text-align: justify; font-size: 1.2em;'><b>Sample Input 2:</b>
  <br>
  Kavezemba 2001-11-18 1015-2
  <br/><b>Sample Output 2:</b>
  <br/>Good day Kavezemba welcome to the Ministry of Home Affairs and Immigration automated system. 
<br/>Your ID number is : <i>011118 1015-2</i>  
<br/>Passport code : <i>P-NAM 011118 5-2</i><br/><br/> <b><i>[Note: There are spaces between the generated id number and postfix or prefix digit]</i></b> </p>









